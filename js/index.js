var nostrChannelEvent;
var nanoAddressStreamer;

var nanoNodes = [];
var verifiedMsgs = [];

function checkStorage() {
  return localStorage.getItem("nano_pvt") && 
    localStorage.getItem("nano_pub") &&
    localStorage.getItem("nostr");
    // localStorage.getItem("nano_wallet") &&
}

function generateIdentity() {
  getNanoKeys();
  getNostrKeys();
}

async function loadIdentity() {
  if (checkStorage()) {
    
    //Nano
    nanoPvtKey = localStorage.getItem("nano_pvt");
    nanoPubKey = localStorage.getItem("nano_pub");
    nanoAccount = NanocurrencyWeb.tools.publicKeyToAddress(nanoPubKey);
    
    // nanoWallet = localStorage.getItem("nano_wallet");

    // Nostr
    nostrPvtKey = localStorage.getItem("nostr");
    nostrRelays = localStorage.getItem("nostrRelays");

    // APIs
    invidiousInstances = JSON.parse(localStorage.getItem("invidiousInstances"));
    nanoNodes = JSON.parse(localStorage.getItem("nanoNodes"));

  }
  else {
    generateIdentity();
  }
}

AbortSignal.timeout ??= function timeout(ms) {
  const ctrl = new AbortController()
  setTimeout(() => ctrl.abort(), ms)
  return ctrl.signal
}

async function generateStreamCode(videoUrl) {
  videoId = videoUrl.split('=')[1];

  if (videoId == undefined) {
    videoId = videoUrl.split("/").slice(-1).pop();
  }

  channelId = localStorage.getItem("channelId");

  apiUrl = invidiousInstance;
  api = apiUrl + ( channelId === null 
    ? "/api/v1/videos/" + videoId 
    : "/api/v1/channels/" + channelId + "/streams" 
  );

  try {
    var live = await(await fetch(api, { signal: AbortSignal.timeout(1000) })).json();
  } catch (error) {
  }

  loadDefaultInvidiousInstances();
  loadInvidiousInstances();

  for (let index = 0; index < invidiousInstances.length; index++) {
    invidiousInstance = invidiousInstances[index];

    try {
      api = invidiousInstance + ( channelId === null 
        ? "/api/v1/videos/" + videoId 
        : "/api/v1/channels/" + channelId + "/streams" 
      );

      live = await(await fetch(api, { signal: AbortSignal.timeout(1000) })).json();
    } catch (ex) {
      continue;
    }

    if (live.videos != undefined) {
      break;
    }
  }

  if (channelId != null) {
    live = live.videos[0];
  }

  localStorage.setItem("channelId", live.authorId);

  nostrChannelEvent = await createPublicChatChannel(live.title, live.description, live.videoThumbnails[5].url);
  nanoAddressStreamer = nanoAccount;

  return NostrTools.nip19.neventEncode(nostrChannelEvent) + nanoAccount;
}

function readStreamCode(streamCode) {
  var tmp = streamCode.split("nano_");

  nostrChannelEvent = NostrTools.nip19.decode(tmp[0]);
  nanoAddressStreamer = "nano_" + tmp[1];
}

function renderQRCode(text) {
  const settings = {
    "text": text,
    "radius": 0.5,
    "ecLevel": "H",
    "fill": "#000000",
    "background": null,
    "size": "250"
  };

  QrCreator.render(settings, document.querySelector("#qrCodeImg"));
}

function clearOldMessages(max) {
  var lst = document.getElementById("lstMsgs");
  var msgs = lst.children;
  while (msgs.length > max) {
    lst.removeChild(msgs[0]);
  }
}

function printMessage(signature, source, amount, msg, author, date, maxchars) {
  if (verify(source, signature, msg)) {
    var lst = document.getElementById("lstMsgs");
    var hms = date.toTimeString().split(' ')[0];

    var div = document.createElement("div");
    div.classList.add("container");

    var authorTxt = document.createElement("strong");
    authorTxt.innerText = author.trim().substring(0, maxchars);

    var msgTxt = document.createElement("p");
    msgTxt.innerText = msg.trim().substring(0, maxchars);

    var timeTxt = document.createElement("span");
    timeTxt.classList.add("time-right");
    timeTxt.innerText = hms;

    div.appendChild(authorTxt);
    div.appendChild(msgTxt);
    div.appendChild(timeTxt);
    lst.insertBefore(div, lst.firstChild);
  }
}

async function loadPaidMsgs() {
  maxMsgs = 1000;
  maxchars = 300;
  if (nostrChannelEvent != undefined) {
    var msgs = await listMessages(nostrChannelEvent.data != undefined ? nostrChannelEvent.data.id : nostrChannelEvent.id);
    var receivables = getReceivables("receivable", nanoAddressStreamer);
    var history = (await getHistory(nanoAddressStreamer)).history;

    if (receivables.blocks == undefined || receivables.blocks.length == 0)
    receivables = getReceivables("pending", nanoAddressStreamer);

    var hashes = [];

    //Math.floor(Date.now() / 1000) - transaction.local_timestamp <= 60 * 5 &&
    for (transaction of history) {
      if (transaction.confirmed == "true" && transaction.type == "receive")
      hashes.push(transaction.hash);
    }

    blocksInfo = await getBlocksInfo(hashes);

    for (var msg of msgs) {
      var json = JSON.parse(msg.content);
      var author = json.author;
      var content = json.content.split('.');
      var signature = content[0];
      var timestamp = content[1];
      var blockHash = content[2];
      var msgTxt = json.content.substring(json.content.indexOf('.', json.content.indexOf(content[2]) + 1) + 1, json.content.length);
      var date = new Date(timestamp * 1000);

      if (blockHash == 'undefined')
      continue;

      var blockInfo = await getBlockInfo(blockHash);

      base64 = btoa(blockHash + msgTxt);

      /* for (const hash of hashes) {
        var block = blocksInfo.blocks[hash];
        if (block.contents.link == blockHash &&
          !verifiedMsgs.includes(base64)
        ) {
          verifiedMsgs.push(base64);
          printMessage(signature, block.source_account, block.amount, msgTxt, author, date, maxchars);
          break;
        }
      } */

      if (verify(nanoAddressStreamer, signature, msgTxt) && !verifiedMsgs.includes(base64)) {
        verifiedMsgs.push(base64);
        printMessage(signature, nanoAddressStreamer, 0, msgTxt, author, date, maxchars);
      }

      if (blockInfo != undefined && blockInfo.block_account != undefined && verify(blockInfo.block_account, signature, msgTxt) && !verifiedMsgs.includes(base64)) {
        verifiedMsgs.push(base64);
        printMessage(signature, blockInfo.block_account, 0, msgTxt, author, date, maxchars);
      }
    }
    clearOldMessages(maxMsgs);
  }

}

function listenForMessages() {
  setInterval(async () => loadPaidMsgs(), 5000);
}

function loadNostrRelaysList() {
  var lst = document.getElementById("nostrRelays");
  loadNostrRelays();

  lst.innerText = "";
  for (const relay of nostrRelays) {

    var rowDiv = document.createElement("div");
    var relayNameDiv = document.createElement("div");
    var removeBtnDiv = document.createElement("div");
    var removeBtn = document.createElement("input");

    removeBtn.type = "button";
    removeBtn.classList.add("pure-button");
    removeBtn.classList.add("button-error");
    removeBtn.classList.add("round");
    removeBtn.addEventListener("click", () => removeNostrRelay(relay));

    removeBtn.value = "Remover";

    relayNameDiv.innerText = relay.length > 25 ? relay.substring(0, 23) + "..." : relay;
    removeBtnDiv.appendChild(removeBtn);

    rowDiv.appendChild(relayNameDiv);
    rowDiv.appendChild(removeBtnDiv);

    lst.appendChild(rowDiv);
  }
}

function loadNanoNodesList() {
  var lst = document.getElementById("nanoNodes");
  loadNanoNodes();

  lst.innerText = "";
  for (const node of nanoNodes) {

    var rowDiv = document.createElement("div");
    var nodeNameDiv = document.createElement("div");
    var removeBtnDiv = document.createElement("div");
    var removeBtn = document.createElement("input");

    removeBtn.type = "button";
    removeBtn.classList.add("pure-button");
    removeBtn.classList.add("button-error");
    removeBtn.classList.add("round");
    removeBtn.addEventListener("click", () => removeNanoNode(node));

    removeBtn.value = "Remover";

    nodeNameDiv.innerText = node.length > 25 ? node.substring(0, 23) + "..." : node;
    removeBtnDiv.appendChild(removeBtn);

    rowDiv.appendChild(nodeNameDiv);
    rowDiv.appendChild(removeBtnDiv);

    lst.appendChild(rowDiv);
  }
}

function loadInvidiousInstancesList() {
  var lst = document.getElementById("invidiousInstances");
  loadInvidiousInstances();

  lst.innerText = "";
  for (const instance of invidiousInstances) {

    var rowDiv = document.createElement("div");
    var instanceNameDiv = document.createElement("div");
    var removeBtnDiv = document.createElement("div");
    var removeBtn = document.createElement("input");

    removeBtn.type = "button";
    removeBtn.classList.add("pure-button");
    removeBtn.classList.add("button-error");
    removeBtn.classList.add("round");
    removeBtn.addEventListener("click", () => removeInvidiousInstance(instance));

    removeBtn.value = "Remover";

    instanceNameDiv.innerText = instance.length > 25 ? instance.substring(0, 23) + "..." : instance;
    removeBtnDiv.appendChild(removeBtn);

    rowDiv.appendChild(instanceNameDiv);
    rowDiv.appendChild(removeBtnDiv);

    lst.appendChild(rowDiv);
  }
}

function loadTabs() {
  const tabs = document.querySelectorAll(".tab");
  const contents = document.querySelectorAll(".content");
  tabs.forEach((tab, index) => {
    tab.addEventListener('click', (e) => {
      tabs.forEach(tab => {
        tab.classList.remove('active');
      });
      tab.classList.add('active');

      contents.forEach(content => {
        content.classList.remove('active');
      });
      contents[index].classList.add('active');
    });
  });
}
