var invidiousInstance = "https://iv.ggtyler.dev";
var invidiousInstances = [];

function loadDefaultInvidiousInstances() {

  loadInvidiousInstances();

  if (invidiousInstances.length > 0)
  return;

  addInvidiousInstance("https://yewtu.be");
  addInvidiousInstance("https://vid.puffyan.us");
  addInvidiousInstance("https://yt.artemislena.eu");
  addInvidiousInstance("https://invidious.flokinet.to");
  addInvidiousInstance("https://invidious.projectsegfau.lt");
  addInvidiousInstance("https://invidious.slipfox.xyz");
  addInvidiousInstance("https://invidious.privacydev.net");
  addInvidiousInstance("https://iv.melmac.space");
  addInvidiousInstance("https://iv.ggtyler.dev");
  addInvidiousInstance("https://invidious.lunar.icu");
  addInvidiousInstance("https://inv.nadeko.net");
  addInvidiousInstance("https://inv.tux.pizza");
  addInvidiousInstance("https://invidious.protokolla.fi");
  addInvidiousInstance("https://iv.nboeck.de");
  addInvidiousInstance("https://invidious.private.coffee");
  addInvidiousInstance("https://yt.drgnz.club");
  addInvidiousInstance("https://iv.datura.network");
  addInvidiousInstance("https://invidious.fdn.fr");
  addInvidiousInstance("https://invidious.perennialte.ch");
  addInvidiousInstance("https://yt.cdaut.de");
  addInvidiousInstance("https://invidious.drgns.space");
  addInvidiousInstance("https://inv.us.projectsegfau.lt");
  addInvidiousInstance("https://invidious.einfachzocken.eu");
  addInvidiousInstance("https://invidious.nerdvpn.de");
  addInvidiousInstance("https://inv.n8pjl.ca");
  addInvidiousInstance("https://youtube.owacon.moe");
  addInvidiousInstance("https://invidious.jing.rocks");
}

function loadInvidiousInstances() {
  invidiousInstances = JSON.parse(localStorage.getItem("invidiousInstances"));
}

function addInvidiousInstance(instance) {
  loadInvidiousInstances();
  var newInstance = instance == null ? document.getElementById("addInvidiousInstance").value : instance;

  if (newInstance.length == 0)
  return;

  if (!newInstance.startsWith("https://")) {
    newInstance = "https://" + newInstance;
  }

  if (invidiousInstances === null) {
    invidiousInstances = [];
  }

  if (!invidiousInstances.includes(newInstance)) {
    invidiousInstances.push(newInstance);
    localStorage.setItem("invidiousInstances", JSON.stringify(invidiousInstances));
  }

  if (instance == null)
  loadInvidiousInstancesList();
}

function removeInvidiousInstance(instance) {
  invidiousInstances.splice(invidiousInstances.indexOf(instance), 1);
  localStorage.setItem("invidiousInstances", JSON.stringify(invidiousInstances));

  loadInvidiousInstancesList();
}
