// Nano
var nanoPvtKey;
var nanoPubKey;
var nanoAccount;
var nanoWallet;

var nanoNode = '';
var nanoNodes = [ nanoNode ];

function setNode(rpc) {
  nanoNode = rpc;
}

async function loadNanoNodes() {
  nanoNodes = JSON.parse(localStorage.getItem("nanoNodes"));

  for (let i = 0; i < nanoNodes.length; i++) {
    nanoNode = nanoNodes[i];

    var blockCount = await getBlockCount();

    if (blockCount != undefined && blockCount.count != undefined) {
      break;
    }
  }
}

function addNanoNode() {
  loadNanoNodes();
  var newNode = document.getElementById("addNanoNode").value   

  if (newNode.length == 0)
  return;

  if (!newNode.startsWith("https://")) {
    newNode = "https://" + newNode;
  }

  if (nanoNodes === null) {
    nanoNodes = [];
  }

  if (!nanoNodes.includes(newNode)) {
    nanoNodes.push(newNode);
    localStorage.setItem("nanoNodes", JSON.stringify(nanoNodes));
  }

  loadNanoNodesList();
}

function removeNanoNode(node) {
  nanoNodes.splice(nanoNodes.indexOf(node), 1);
  localStorage.setItem("nanoNodes", JSON.stringify(nanoNodes));

  loadNanoNodesList();
}

async function sendNanoRPC(data, node) {

  var payload = data;
  var data = undefined;

  for (let index = 0; index < nanoNodes.length && data == undefined; index++) {
    node = nanoNodes[index];

    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), 5000);

    data = await fetch(node, {
      signal: controller.signal,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(payload),
    }).then(response => response.json())
      .then(data => data)
      .catch((error) => {
      });

  }

  return data;
}

async function getNanoKeys() {
  var data = await sendNanoRPC({ "action": "key_create" }, nanoNode);
  nanoPvtKey = data.private;
  nanoPubKey = data.public;
  nanoAccount = data.account;

  localStorage.setItem("nano_pvt", nanoPvtKey);
  localStorage.setItem("nano_pub", nanoPubKey);
  // localStorage.setItem("nano_wallet", nanoWallet);
}

function sign(msg) {
  var signature = NanocurrencyWeb.tools.sign(nanoPvtKey, msg);
  return signature;
}

function verify(pbKey, signature, msg) {
  if (pbKey.startsWith("nano"))
  pbKey = NanocurrencyWeb.tools.addressToPublicKey(pbKey);
  return NanocurrencyWeb.tools.verify(pbKey, signature, msg);
}

async function createWallet() {
  data = {
    "action": "wallet_create"
  };

  return await sendNanoRPC(data, nanoNode);
}

async function addAccountToWallet(wallet, privKey) {
  data = {
    "action": "wallet_add",
    "wallet": wallet,
    "key": privKey
  };

  return await sendNanoRPC(data, nanoNode);
}

async function getWalletInfo(wallet) {
  data = {
    "action": "wallet_info",
    "wallet": wallet
  };

  return await sendNanoRPC(data, nanoNode);
}

async function getAccountInfo(account) {
  data = {
    "action": "account_info",
    "account": account
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getHistory(account) {
  data = {
    "action": "account_history",
    "account": account,
    "count": 1000000
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getBlockCount() {
  data = {  
    "action": "block_count"
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getBlockInfo(blockHash) {
  data = {  
    "action": "block_info",
    "json_block": "true",
    "source": "true",
    "hash": blockHash
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getBlocksInfo(blockHashes) {
  data = {  
    "action": "blocks_info",
    "json_block": "true",
    "source": "true",
    "hashes": blockHashes
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getRepresentative(address) {
  data = {
    "action": "account_representative",
    "address": address
  }

  return await sendNanoRPC(data, nanoNode);
}

async function getBalance(account) {
  data = {
    "action": "account_balance",
    "account": account
  }

  return await sendNanoRPC(data, nanoNode);
}

async function receiveNano(account) {
  var receivables = await getReceivables("receivable", account);

  if (receivables.blocks == undefined || receivables.blocks == "")
  receivables = await getReceivables("pending", account);

  for (const block in receivables.blocks) {
    data = {
      "action": "receive",
      "wallet": nanoWallet,
      "account": account,
      "block": block
    }
    await sendNanoRPC(data, nanoNode);
  };
}

async function sendNano(srcAccount, destAccount, amount, msgSignature) {
  data = {
    "action": "send",
    "wallet": nanoWallet,
    "source": srcAccount,
    "destination": destAccount,
    "amount": NanocurrencyWeb.tools.convert(amount, 'NANO', 'RAW'),
    "id": msgSignature
  };

  return await sendNanoRPC(data, nanoNode);
}

async function getReceivables(action, account) {
  data = {
    "action": action,
    "account": account,
    "source": "true"
  }

  return await sendNanoRPC(data, nanoNode);
}
