// Nostr
var nostrPvtKey;
var nostrPubKey;

var nostrPublicChannel;

var nostrRelay = "wss://nos.lol";
var nostrRelays = [];

const pool = new NostrTools.SimplePool();
console.log(pool);

function loadNostrRelays() {
  nostrRelays = JSON.parse(localStorage.getItem("nostrRelays"));
}

function addNostrRelay() {
  loadNostrRelays();
  var newRelay = document.getElementById("addNostrRelay").value   

  if (newRelay.length == 0)
    return;

  if (!newRelay.startsWith("wss://")) {
    newRelay = "wss://" + newRelay;
  }

  if (nostrRelays === null) {
    nostrRelays = [];
  }

  if (!nostrRelays.includes(newRelay)) {
    nostrRelays.push(newRelay);
    localStorage.setItem("nostrRelays", JSON.stringify(nostrRelays));
  }

  loadNostrRelaysList();
}

function removeNostrRelay(relay) {
  nostrRelays.splice(nostrRelays.indexOf(relay), 1);
  localStorage.setItem("nostrRelays", JSON.stringify(nostrRelays));

  loadNostrRelaysList();
}

function getNostrKeys() {
  nostrPvtKey = NostrTools.generatePrivateKey();
  nostrPubKey = NostrTools.getPublicKey(nostrPvtKey);

  localStorage.setItem("nostr", nostrPvtKey);
}

// async function connect() {
//   loadNostrRelays();

//   for (let index = 0; index < nostrRelays.length; index++) {
//     relay = NostrTools.relayInit(nostrRelays[index]);
//     console.log(relay);

//     relay.on('connect', () => {
//       console.log('Connected to ' + relay);
//     })
//     relay.on('error', () => {
//       console.log('Failed to connect to ' + relay);
//     })

//     try {
//       await relay.connect();
//     } catch (error) {
//       console.log(1);
//       continue;
//     }

//     console.log('funfou', relay);
//     break;
//   }
// }

// function disconnect() {
//   relay.close()
// }

async function createPublicChatChannel(name, about, picture) {
  // await connect();

  var channelMetadata = {
    name: name,
    about: about,
    picture: picture,
  }

  const template = {
    content: channelMetadata,
    created_at: Math.floor(Date.now() / 1000)
  }

  var event = NostrTools.nip28.channelCreateEvent(template, nostrPvtKey);
  // var signed = NostrTools.finishEvent(event, nostrPvtKey);

  await pool.publish(nostrRelays, event);

  // disconnect();
  return event;
}

async function hideChatMessage(channelMsgEvent, reason) {
  // await connect();
  console.log(channelMsgEvent);

  const template = {
    channel_message_event_id: channelMsgEvent.id,
    content: { reason: reason},
    created_at: Math.floor(Date.now() / 1000)
  };

  var event = NostrTools.nip28.channelHideMessageEvent(template, nostrPvtKey);
  console.log(event);
  console.log(await pool.publish(event));

  // disconnect();
  return event;
}

async function sendNostrMessage(message, chatCreationEvent) {
  // await connect();

  const template = {
    channel_create_event_id: chatCreationEvent.id,
    relay_url: nostrRelay,
    content: message,
    created_at: Math.floor(Date.now() / 1000)
  };

  var event = NostrTools.nip28.channelMessageEvent(template, nostrPvtKey);

  await pool.publish(nostrRelays, event);

  // disconnect();
  return event;
}

async function listMessages(chatId) {
  // await connect();

  if (chatId.startsWith("nevent"))
  chatId = NostrTools.nip19.decode(chatId).data.id;

  var msgs = [];

  await pool.list(nostrRelays, [
    {
      kinds: [42, 43],
      '#e': [chatId],
    }
  ]).then(event => {
    // console.log(event);
    // msgs.push(event);
    msgs = event;
  });

  // disconnect();

  return msgs.reverse();
}
